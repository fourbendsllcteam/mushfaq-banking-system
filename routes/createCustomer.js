const express = require("express");
var router = express.Router();
const jwt =require('jsonwebtoken');
const PGS = require('../db_connect/db');

router.get('/customers', verifyToken,(req,res) =>{
    
    jwt.verify(req.token,'secretkey ',(err,authdata) => {
        if(err){
            res.json({
            jsonOutput:{
            "result": 
            {  
                
            },
            "message": 
            {   
                "ErrorCode" : 403, 
                "ErrorMessage" : "Unauthorized access"
            }
        }
    });

        }else{
            res.json({
                  authdata,  
                jsonOutput:{
                    "message" :{ 
                        "ErrorCode" : 0, 
                        "ErrorMessage" : "Successfully Loggedin"
                    }
                }
            
            });
            }
        }
    );
})

            // if(err)
            // {
            //     res.sendStatus(403);
            // }
            // else{
            //     res.json({
            //         authdata
            //     });
            // }


router.post("/customers/reg", async (req, res) => {

  const newcustomer = {
    name : req.body.name,
    email :req.body.email,
    pwd: req.body.pwd,
    gender: req.body.gender,
    address: req.body.address,
    dob: req.body.dob,

    mobilenumber: req.body.mobilenumber,
    status: "Successfully created new customer",
  };

  res.json(newcustomer);
});


function verifyToken(req,res,next){
    const bearerHeader =req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split( ' ' );
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else{
        res.sendStatus(404);
    }
}

module.exports = router;

