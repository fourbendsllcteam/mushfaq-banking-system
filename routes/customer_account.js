const bcrypt = require('bcrypt');
const express =require('express');
const PGS = require('../db_connect/db');
const router= express.Router();

// create users
router.post('/ca/insert',async(req,res)=>{
	const {user_id,customer_id,account_type,initial_deposit} = req.body;
	let insertQuery = await PGS.execute_query (`INSERT INTO customers_account (
            user_id,customers_id,account_type,initial_deposit
        ) VALUES ('${user_id}','${customer_id}','${account_type}','${initial_deposit}')`);
			
	console.log('query>>>>>>>>',insertQuery);

	res.status(200).send({message:"added successfully"});

});

//read
router.get('/ca/read',async(req,res)=>{

	const readQuery = await PGS.execute_query(
		"SELECT * FROM users "
	  );

   
   console.log('query>>>>>>>>',readQuery);

	res.json(readQuery);
});

//update
router.put('/ca/update',async(req,res)=>{
	
	const {user_id,customer_id,account_type,initial_deposit} = req.body;
		

	//  let updateQuery = await PGS.execute_query(
	//  	"UPDATE users SET username = $1, firstname = $2, lastname = $3 ,  email = $4 WHERE username = $1",
	//  	[username, firstname, lastname, email]);
	let updateQuery =await PGS.execute_query (`UPDATE customers_account SET customers_id ='${customer_id}', account_type = '${account_type}', initial_deposit = '${initial_deposit}'
				WHERE user_id='${user_id}'`);
	
	console.log('query>>>>',updateQuery);

	res.status(200).send({message:"updated successfully"});

});

//delete
router.delete('/ca/delete',async(req,res)=>{

	const {customers_id} = req.body;
	const deleteQuery = await PGS.execute_query(`DELETE FROM customers_account WHERE customers_id = '${customers_id}'`);

	console.log('deleteQuery>>>>>>>>',deleteQuery);

  	res.status(200).send({ message: 'Product deleted successfully!', customers_id });

});
module.exports = router;