const db = require('../db_connect/db');

const bcrypt = require('bcrypt');
const express =require('express');
const PGS = require('../db_connect/db');
const router= express.Router();

// create users
router.post('/newuser/insert',async(req,res)=>{
	const {user_id,username, firstname, lastname, email} = req.body;
	const created_time= new Date().toUTCString();
	const salt = await bcrypt.genSalt(10);
	const password = await bcrypt.hash(req.body.password, salt);
	// let query = await PGS.execute_query(
	// 	"INSERT INTO users (uuid,username, firstname, lastname,email,password) VALUES ($1, $2, $3, $4, $5, $6)",
    // 	[uuid,username, firstname, lastname,email,password] );
	let insertQuery = await PGS.execute_query (`INSERT INTO users (
		user_id,username,firstname,lastname,email,password,created_time
	) VALUES ('${user_id}','${username}','${firstname}','${lastname}','${email}','${password}','${created_time}')`);

			
	console.log('query>>>>>>>>',insertQuery);

	res.status(200).send({message:"added successfully"});

});

//Read users

router.get('/newuser/read',async(req,res)=>{

	const readQuery = await PGS.execute_query(
		"SELECT * FROM users "
	  );

   
   console.log('query>>>>>>>>',readQuery);

	res.json(readQuery);
});

//update users

router.put('/newuser/update',async(req,res)=>{
	
	const {username, firstname, lastname, email,user_id} = req.body;
		

	//  let updateQuery = await PGS.execute_query(
	//  	"UPDATE users SET username = $1, firstname = $2, lastname = $3 ,  email = $4 WHERE username = $1",
	//  	[username, firstname, lastname, email]);
	let updateQuery =await PGS.execute_query (`UPDATE users SET  username = '${username}', firstname = '${firstname}' , lastname = '${lastname}' , 
				email = '${email}'
				WHERE user_id='${user_id}'`);
	
	console.log('query>>>>',updateQuery);

	res.status(200).send({message:"updated successfully"});

});

//delete users

router.delete('/newuser/delete',async(req,res)=>{

	const {username} = req.body;
	const deleteQuery = await PGS.execute_query(`DELETE FROM users WHERE username = '${username}'`);

	console.log('deleteQuery>>>>>>>>',deleteQuery);

  	res.status(200).send({ message: 'Product deleted successfully!', username });

});

module.exports = router;
