var express = require("express");
var router = express.Router();
const PGS = require('../db_connect/db');
const jwt=require('jsonwebtoken');
const bcrypt = require('bcrypt');



//login
router.post('/main',async(req, res) => {

    let username = req.body.username;
    let password = req.body.password;
    console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>',req.params);
    console.log('>>>>>>>>>>',username);
    console.log('>>>>>>>>>>',password);
    
    var payload = {
         username ,
    }
    var jwtoken=jwt.sign(payload,'secretkey');
    
    let queryStr = `SELECT * FROM users where username='${username}'`;
    let queryResult = await PGS.execute_query(queryStr);
    let jsonOutput = {
        "result" : {
            "token" :"",
            "username" : "",
            "email" : ""
        },
        "message" : {
            "ErrorCode" : 2,
            "ErrorMessage" : "Connection Issue"
        }
    };
   

    if(queryResult.status==1)
    {
        let res = queryResult.result;
        // console.log('>>>>>>>>>>',queryResult);
        let urec = res.rows[0];
        console.log(urec);
        let dbpassword = urec.password; 
        console.log('>>>>>>>>>>',password,dbpassword);
        console.log(password.length);
        const matchPasswd = await bcrypt.compare(password, dbpassword);

        console.log(matchPasswd)
    
        if(matchPasswd){
            jsonOutput["result"] = { "token": jwtoken, "username" : urec.username, "email": urec.email};
            jsonOutput["message"] = {"ErrorCode" : 0, "ErrorMessage" : "Successfully Loggedin"};
        }else{
            jsonOutput["result"] = { "token": "", "username" :"wrong", "email":""};
            jsonOutput["message"] = {"ErrorCode" : 3, "ErrorMessage" : "Invalid User"};
        }
    } res.json(jsonOutput);

 });
module.exports = router;