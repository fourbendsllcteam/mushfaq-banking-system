const Pool = require('pg').Pool;

const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'api',
  password: 'gokul123',
  port: 5432,
});

pool.on('error', (err, client) => {
    console.error('Unexpected error on idle client', err)
    process.exit(-1)
  })


var PGS = {

    execute_query :(queryStr) => {
        let queryResult = {
            status : 1,
            result : ""
        };
        return pool
            .connect()
            .then(pool => {
                return pool
                .query(queryStr)
                .then(res => {
                    pool.release()
                    console.log(res);
                    queryResult.status = 1;
                    queryResult.result = res;
                    return queryResult;
                })
                .catch(err => {
                    pool.release()
                    console.log(err.stack);
                    return queryResult;
                })
            });
    }
    

};

module.exports = PGS;